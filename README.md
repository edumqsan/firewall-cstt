 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

 Configure packets filtering rules with the iptables tool automatically.

 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

## Usage

( To run this software you do not need to install it. )

#### Clone the repository.

    $ git clone https://github.com/edumqsan/firewall-cstt.git

#### Run with root privilege

Logged in your root account type:

    # ./firewall-cstt

or with sudo:

    $ sudo ./firewall-cstt


### License

MIT License
